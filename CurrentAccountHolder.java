package bankingApp;

import java.io.IOException;

public class CurrentAccountHolder implements rateCalculator{
    public String[] currentData;
    public String accNum;
    public int num;

    @Override
    public void getAccountNumber(String row) throws IOException {
        {
            currentData = row.split(",");
            accNum = "2" + (utility.trimNum(currentData[1]) + utility.generateUniqueNum(num) + utility.RandNum());

        }
    }

    @Override
    public Double calculateRate() {
        double savingInterest = (Double.valueOf(currentData[3]))*(((baseRate*20)/100))*1;
        return savingInterest;
    }

    @Override
    public void showDetails()
    {
        System.out.println();
        System.out.println("Requested Customer details are:");
        System.out.println("Customer Name : " +currentData[0]);
        System.out.println("Customer Account Number : "+accNum);
        System.out.println("Customer Aadhar Card Number : "+currentData[1]);
        System.out.println("Customer Account Type : "+currentData[2]);
        System.out.println("Customer Initial Deposited Amount : "+currentData[3]);
        long debitCardNo = utility.getDebitCardnum();
        System.out.println("The Debit Card Number for "+currentData[0]+" is : "+debitCardNo);
        int accessCode = utility.getAccessCode();
        System.out.println("The Access Code for Locker ID "+debitCardNo+" is : "+accessCode);
        System.out.println();
        System.out.println("As per initial deposited amount and current base rate, total interest will get for a year : "+calculateRate());
    }

}


