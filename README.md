Below are the project requirments:

Develop an Bank Account Management System for a sample bank with
following details.
- Bank supports two types of accounts
	- Savings A/c
	- Current A/c
- 12 digit Account Number is assigned to the person
	- For Savings/Current Account - 1/2 + last 3 digit of Aadhar No. + unique 5 digit No
			+ random 3-digit no.
- Saving Account holders are given a Locker having 3-digit Locker ID and 4-digit Access code
- Current Account holders are given one debit card with 16-digit number,
	4-digit PIN

- Both accounts will use an interface that determines the base interest rate
    	- Rate for Savings = .5 points less than the base rate
	- Rate for current  = 20% of the base Rate
- The showDetails() method should reveal relevant account information as well as information specific to the Savings acc or current acc.  


Learning Objectives
- Abstract class/methods 
- Interfaces
- Constructors, super()
- Access modifiers   
	- public, private, protected
- Read data from a csv file in the format
	Name, 12-digit Aadhar No., Acc type, Initial Deposit
- String class methods
- Random number generation.

uniq 5 digit number - Done
random 3 digit number - Done
trim last 3 adhar digit - Done
build 12 digit acc number for SAving & current
3 digit locker id & 4 digit access code for saving account
16 digit debit card number to current acc holder & 4 digit PIN

account number - last 3 digit of Adhar,unique 5 digit,random 3 digit no.
