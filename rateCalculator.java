package bankingApp;

import java.io.IOException;

public interface rateCalculator {
    double baseRate = 1.5;
    int duration = 1;
    Double calculateRate();
    void getAccountNumber(String str) throws IOException;
    void showDetails();
}
