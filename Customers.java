package bankingApp;

import java.io.*;
import java.util.Scanner;

public class Customers {

    public String custName;
    public FileReader fr;
    public static BufferedReader br;

    public static void main(String[] args) throws IOException {
        String row;
        FileReader fr = new FileReader("D:\\FINASTRA SELENIUM\\FirstProjectData\\CSV_Data.txt");
        br = new BufferedReader(fr);

        System.out.println("Please Enter Customer first name and last name :" + "\n");
        Scanner sn = new Scanner(System.in);
        String custName = sn.nextLine();
        SavingAccountHolder sc = new SavingAccountHolder();
        CurrentAccountHolder cr = new CurrentAccountHolder();

        while ((row = br.readLine()) != null) {
            if (row.contains(custName) && row.contains("Saving")) {
                sc.getAccountNumber(row);
                sc.showDetails();
            } else if (row.contains(custName) && row.contains("Current")) {
                cr.getAccountNumber(row);
                cr.showDetails();
            }
        }
    }
}




